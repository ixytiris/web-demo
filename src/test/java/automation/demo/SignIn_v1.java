package automation.demo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by ichitiris on 11/10/17.
 * V1: no abstraction layers
 *
 * Objective:
 * Test validation message
 */
public class SignIn_v1 {

    WebDriver       driver;

    @Parameters({"browser"})
    @BeforeClass(description = "Open browser")
    public void startBrowser(String browser){
        if(browser.equals("chrome")) {
            driver = new ChromeDriver();
        }else{
            driver = new FirefoxDriver();
        }
        driver.get("http://www.phptravels.net/admin");
        driver.switchTo().window(driver.getWindowHandle()).manage().window().maximize();
    }

    @Test(description = "Verify error message in case of invalid pwd")
    public void invalidLogin() {
        driver.findElement(By.name("email")).sendKeys("admin@phptravels.com");//Type username
        driver.findElement(By.name("password")).sendKeys("wrongPassword");//Type pwd
        driver.findElement(By.xpath("//span[text()='SignUp']")).click();//Click login

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(@class, 'alert') and text()='Invalid Login Credentials']")));
        String errorMessage = driver.findElement(By.xpath("//div[contains(@div, 'alert')]")).getText();//Get error message text

        //Validate error message
        Assert.assertEquals(errorMessage,"Invalid Login Credentials");
    }

    @AfterClass(description = "Close browser")
    public void closeBrowser(){
        driver.quit();
    }

}

