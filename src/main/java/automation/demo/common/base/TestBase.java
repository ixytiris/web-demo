package automation.demo.common.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

/**
 * Created by ichitiris on 11/13/17.
 *
 * Responsible to open/close the browser
 */
public class TestBase {

    public WebDriver driver;

}
