package automation.demo.pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Page Object model:
 * a) store web elements
 * b) store locators
 * c) store actions
 * Created by ichitiris on 11/10/17.
 */
public class SignInPageObject {

    final WebDriver driver;

    public SignInPageObject(WebDriver driver){
        this.driver = driver;
    }

    //WEB ELEMENTS
    @FindBy(how = How.NAME, using = "email")
    public WebElement emailInput;

    //LOCATOR
    public String errorMessage = "//div[contains(@class, 'alert') and text()='Invalid Login Credentials']";


    //ACTIONS
    public void inputEmailAddress(String email){emailInput.sendKeys(email);}


    //EXPLICIT WAIT
    public void waitForErrorText(){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(errorMessage)));
    }

}
