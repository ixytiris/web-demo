package automation.demo.businessObjects;

import automation.demo.pageObjects.SignInPageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by ichitiris on 11/16/17.
 * Class that capture the business logic
 */
public class SignInBusinessObject {

    SignInPageObject signIn;
    WebDriver driver;

    public SignInBusinessObject(WebDriver driver){
        this.driver=driver;
    }

    public String signInGetErrorMessage(String username,String pwd){
        return "";
    }
}
